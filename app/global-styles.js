import {injectGlobal} from 'styled-components'

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'KozGoPr6N-Regular', 'Hiragino Kaku Gothic ProN', 'ヒラギノ角ゴ Pro W3', "游ゴシック体", "Yu Gothic", YuGothic, 'メイリオ', Meiryo, 'ＭＳ Ｐゴシック', sans-serif
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: 'KozGoPr6N-Regular', 'Hiragino Kaku Gothic ProN', 'ヒラギノ角ゴ Pro W3', "游ゴシック体", "Yu Gothic", YuGothic, 'メイリオ', Meiryo, 'ＭＳ Ｐゴシック', sans-serif;
    line-height: 1.5em;
  }
`
