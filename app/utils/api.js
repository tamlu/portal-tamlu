import {request} from 'utils/request'
import qs from 'qs'

export const postLoginUser = ({email, password}) =>
  request('/auth', {
    method: 'POST',
    body: JSON.stringify({
      email,
      password
    }),
    headers: new Headers({
      'Content-Type': 'application/json'
    })
  })

export const postSignupUser = ({email, password}) =>
  request('/users', {
    method: 'POST',
    body: JSON.stringify({
      email,
      password
    }),
    headers: new Headers({
      'Content-Type': 'application/json'
    })
  })

export const postRequestToJudge = params =>
  request('/sponsee-request', {
    method: 'POST',
    body: JSON.stringify(params),
    headers: new Headers({
      'Content-Type': 'application/json'
    })
  })
export const getUser = ({access_token, id}) =>
  request(`/users/${id}?${qs.stringify({access_token})}`)
