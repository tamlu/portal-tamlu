export default {
  yellow: '#FEEC42',
  pink: '#FE5B6B',
  darkPink: '#FE283D',
  redFlamePea: '#E15146',
  orange: '#FEBF42',
  green: '#39DB72',
  greenFruitSalad: '#48A25A',
  blue: '#13ACFE',
  purple: '#BD74FD',
  lightGray: '#E6E6E6',
  mediumGray: '#CACACA',
  darkGray: '#8A8A8A',
  lightDark: '#333333',
  black: '#0A0A0A',
  ecruWhite: '#FAF9F2',
  white: '#FEFEFE',

  whiteAbsolute: '#FFFFFF',
  blackAbsolute: '#000000',
  inputBorder: '#E0DFD6',
  inputPlaceHolder: '#E5E4DC',
  boxBorder: '#EBEBEB'
}
