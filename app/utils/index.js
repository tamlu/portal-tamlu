export const isClient = () => process.env.BROWSER
export const isExpired = expiredAt => {
  if (!expiredAt) {
    return true
  }
  const expiredTime = new Date(expiredAt)
  return expiredTime <= new Date()
}
