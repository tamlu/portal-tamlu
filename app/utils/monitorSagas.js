export default function monitorSagas(store) {
  const allTasks = []
  const saveRunSaga = store.runSaga
  /*eslint-disable no-param-reassign*/
  store.runSaga = function interceptRunSaga(saga) {
    // eslint-disable-line no-param-reassign
    const task = saveRunSaga.call(store, saga)
    allTasks.push(task)
    return task
  }
  /*eslint-enable no-param-reassign*/
  return function done() {
    return Promise.all(allTasks.map(t => t.done))
  }
}
