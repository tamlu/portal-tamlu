import {put, call} from 'redux-saga/effects'
import {
  startPageTransition,
  endPageTransition,
  showNotificationAction
} from 'containers/App/actions'

export default function* sendRequest(entityActions, apiFn, ...payload) {
  yield put(startPageTransition())
  yield put(entityActions.request({payload}))
  const {response, error} = yield call(apiFn, ...payload)
  if (response) {
    yield put(entityActions.success({response, payload}))
  }
  else {
    yield put(entityActions.failure({error, payload}))
    yield put(
      showNotificationAction({
        message: 'Error',
        description: error
      })
    )
  }
  yield put(endPageTransition())
}
