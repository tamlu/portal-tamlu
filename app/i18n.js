import {addLocaleData} from 'react-intl'
import enLocaleData from 'react-intl/locale-data/en'
import jaLocaleData from 'react-intl/locale-data/ja'
import zhLocaleData from 'react-intl/locale-data/zh'

import { DEFAULT_LOCALE } from './containers/App/constants'// eslint-disable-line

import enTranslationMessages from './translations/en.json'
import jaTranslationMessages from './translations/ja.json'
import zhTranslationMessages from './translations/zh.json'

[enLocaleData, jaLocaleData, zhLocaleData].forEach(addLocaleData)

export const appLocales = ['ja', 'en', 'zh']

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages = locale !== DEFAULT_LOCALE
    ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
    : {}
  return Object.keys(messages).reduce((formattedMessages, key) => {
    let message = messages[key]
    if (!message && locale !== DEFAULT_LOCALE) {
      message = defaultFormattedMessages[key]
    }
    return Object.assign(formattedMessages, {[key]: message})
  }, {})
}

export const translationMessages = {
  en: formatTranslationMessages('en', enTranslationMessages),
  ja: formatTranslationMessages('ja', jaTranslationMessages),
  zh: formatTranslationMessages('zh', zhTranslationMessages)
}
