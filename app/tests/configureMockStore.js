import { createStore } from 'redux';
import { fromJS } from 'immutable';

const createReducer = () => (state) => state;

export default (initialState = {}, rootReducer = createReducer()) => (
  createStore(
    rootReducer,
    fromJS(initialState)
  )
);
