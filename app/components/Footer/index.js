import React from 'react'
import {Link} from 'react-router'
import {Icon} from 'antd'
import styled from 'styled-components'
import LocaleToggle from '../../containers/LocaleToggle'
import {colors, typeface, layout, media} from '../../utils/styles'

const FooterWrapper = styled.footer`
  ${layout.center}
  ${layout.col}
  color: ${colors.white};
  background-color: ${colors.black};
  width: 100%;
`
const LinkWrapper = styled.ul`
  ${layout.flex}
  justify-content: space-between;
  flex-wrap: wrap;
  width: 100%;
  max-width: 1000px;
`
const LinkGroup = styled.li`
  padding: 20px;
  ${media.tablet`
    width: 100%;
  `}
`
const GroupHeader = styled.h3`
  font-weight: normal;
  font-size: ${typeface.size.normal}
  margin-bottom: 10px;
  ${media.tablet`
    text-align: center;
  `}
`
const GroupIcon = styled(Icon)`
  margin-right: 10px;
`
const LinkItem = styled(Link)`
  display: block;
  padding: 3px;
  ${media.tablet`
    text-align: center;
  `}
`
const Divider = styled.div`
  height: 1px;
  width: 100%;
  max-width: 1000px;
  border: none;
  background-color: ${colors.mediumGray}
`
const BottomBar = styled.div`
  ${layout.verticalCenter}
  justify-content: space-between;
  width: 100%;
  max-width: 1000px;
  padding: 20px;
`
const Copyright = styled(Link)`
  font-weight: 400;
  font-size: ${typeface.size.smaller};
  color: ${colors.white};
`
const Footer = () =>
  <FooterWrapper>
    <LinkWrapper>
      <LinkGroup>
        <GroupHeader>
          <GroupIcon type='info-circle-o' />
          Send request for coin creation
        </GroupHeader>
        <LinkItem to='/'>Send request for coin creation</LinkItem>
        <LinkItem to='/'>What is coin?</LinkItem>
        <LinkItem to='/'>How to create coin?</LinkItem>
        <LinkItem to='/'>Guideline</LinkItem>
        <LinkItem to='/'>How to exchange from ETH to cash?</LinkItem>
        <LinkItem to='/'>How to use coin?</LinkItem>
        <LinkItem to='/'>Know more about ETH</LinkItem>
      </LinkGroup>
      <LinkGroup>
        <GroupHeader>
          <GroupIcon type='search' />
          Search
        </GroupHeader>
        <LinkItem to='/'>Man</LinkItem>
        <LinkItem to='/'>Woman</LinkItem>
        <LinkItem to='/'>Group</LinkItem>
        <LinkItem to='/'>Company</LinkItem>
      </LinkGroup>
      <LinkGroup>
        <GroupHeader>
          <GroupIcon type='question-circle-o' />
          About REALBOOST
        </GroupHeader>
        <LinkItem to='/'>What is REALBOOST?</LinkItem>
        <LinkItem to='/'>FAQ</LinkItem>
        <LinkItem to='/'>Term of Use</LinkItem>
        <LinkItem to='/'>Privacy Policy</LinkItem>
        <LinkItem to='/'>Operating Company</LinkItem>
      </LinkGroup>
    </LinkWrapper>
    <Divider />
    <BottomBar>
      <Copyright to='/'>
        © 2017 Real Boost.
      </Copyright>
      <LocaleToggle />
    </BottomBar>
  </FooterWrapper>

export default Footer
