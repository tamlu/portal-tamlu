import React, {PropTypes} from 'react'
import {Link} from 'react-router'
import {Input, Icon, Menu, Dropdown, Avatar, Badge, Button} from 'antd'
import styled from 'styled-components'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import {makeSelectNotification} from 'containers/App/selectors'
import {media, colors, layout} from '../../utils/styles'
import {zIndexes} from '../../utils/constants'
import logo from '../../assets/image/logo.svg'
import openNotification from './Notification'

const StyledHeader = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: ${zIndexes.header};
  ${layout.verticalCenter}
  justify-content: space-between;
  background-color: ${colors.yellow};
  padding: 0 20px;
  ${media.tablet`
    padding: 0 5px;
  `}
`
const SearchWrapper = styled.div`
  height: 55px;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  padding: 0 10px;
  z-index: ${zIndexes.header};
  ${layout.center}
  i {
    font-size: 20px;
    cursor: pointer;
  }
`
const SearchInput = styled(Input.Search)`
  height: 55px;
  border: none !important;
  box-shadow: none !important;
  padding: 5px 35px !important;
`
const LHSWrapper = styled.span`
  ${layout.verticalCenter}
  flex-wrap: nowrap;
  flex: 1 1 auto;
`
const RHSWrapper = styled.span`
  ${layout.verticalCenter}
  flex-wrap: nowrap;
  flex: 0 1 auto;
`
const Logo = styled.img`
  height: 55px;
  width: 200px;
  cursor: pointer;
  ${media.tablet`
    width: 100px;
  `}
`
const SearchBar = styled.span`
  flex: 1 1 auto;
  padding: 10px 0 10px 10px;
  display: inline-block;
  ${media.tablet`
    display: none;
  `}
`
const SearchButton = styled(Button)`
  display: none;
  ${media.tablet`
    display: inline-block;
    margin-right: 10px;
  `}
`
const AvatarButton = styled(Avatar)`
  background-color: ${colors.blue};
`
const ButtonsWrapper = styled.span`
  ${layout.verticalCenter}
  flex: nowrap;
  border: none;
  margin-left: 10px;
  border-right: 1px solid ${colors.darkGray};
`
const BadgeButton = styled(Badge)`
  line-height: 0;
  cursor: pointer;
  vertical-align: middle;
  margin-right: 10px;
`
const UserWrapper = styled(Dropdown)`
`
const User = styled.span`
  ${layout.verticalCenter}
  margin-left: 10px;
  cursor: pointer;
`
const UserName = styled.span`
  ${layout.verticalCenter}
  margin: 0 10px;
`
const UserAvatar = styled(Avatar)`
  background-color: ${colors.purple}
`
const MenuItem = styled(Menu.Item)`
  padding: 10px 20px;
`
const MenuIcon = styled(Icon)`
  margin-right: 5px;
`

const userMenu = (
  <Menu>
    <MenuItem>
      <MenuIcon style={{color: colors.purple}} type='user' />
      User Name
    </MenuItem>
    <MenuItem>
      <MenuIcon style={{color: colors.blue}} type='compass' />
      Category
    </MenuItem>
    <MenuItem>
      <MenuIcon style={{color: colors.blue}} type='bell' />
      Notification
    </MenuItem>
    <MenuItem>
      <MenuIcon style={{color: colors.blue}} type='file' />
      My Page (Sponsor)
    </MenuItem>
    <MenuItem>
      <MenuIcon style={{color: colors.pink}} type='file' />
      My Page (Sponsee)
    </MenuItem>
    <MenuItem>
      <MenuIcon type='setting' />
      Security Settings
    </MenuItem>
    <MenuItem>
      <MenuIcon type='logout' />
      Logout
    </MenuItem>
  </Menu>
)

class Header extends React.PureComponent {
  state = {
    openSearching: false
  }

  componentWillReceiveProps(nextprops) {
    if (this.props.notification.rand !== nextprops.notification.rand) {
      openNotification(nextprops.notification)
    }
  }

  _openSearching = () => {
    this.setState({openSearching: true})
  }

  _closeSearching = () => {
    this.setState({openSearching: false})
  }

  render() {
    const {openSearching} = this.state

    return openSearching
      ? <SearchWrapper>
        <SearchInput
          placeholder='Searching for Sponsees'
          prefix={<Icon type='close' onClick={this._closeSearching} />}
        />
      </SearchWrapper>
      : <StyledHeader>
        <LHSWrapper>
          <Link to='/'><Logo src={logo} /></Link>
          <SearchBar>
            <Input.Search placeholder='Searching for Sponsees' />
          </SearchBar>
        </LHSWrapper>
        <RHSWrapper>
          <ButtonsWrapper>
            <SearchButton
              shape='circle'
              icon='search'
              onClick={this._openSearching}
            />
            <BadgeButton>
              <AvatarButton shape='circle' icon='compass' size='small' />
            </BadgeButton>
            <BadgeButton count={5}>
              <AvatarButton shape='circle' icon='bell' size='small' />
            </BadgeButton>
          </ButtonsWrapper>
          <UserWrapper overlay={userMenu} trigger={['click']}>
            <User>
              <UserAvatar icon='user' />
              <UserName>User Name</UserName>
              <Icon type='down' />
            </User>
          </UserWrapper>
        </RHSWrapper>
      </StyledHeader>
  }
}

const mapStateToProps = createStructuredSelector({
  notification: makeSelectNotification()
})

Header.propTypes = {
  notification: PropTypes.object
}

export default connect(mapStateToProps)(Header)
