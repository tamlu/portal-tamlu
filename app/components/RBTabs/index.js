import React from 'react'
import styled from 'styled-components'
import {Tabs} from 'antd'

import {colors} from '../../utils/styles'

const TabPane = Tabs.TabPane

// region ---------- CSS
const Main = styled(Tabs)`
  .ant-tabs-content {
    background-color: ${colors.white};
  }
  .ant-tabs-bar {
    border-bottom: none;
    background-color: ${colors.black};
    border-radius: 4px;
    
    .ant-tabs-nav-container {
      overflow: visible;
      .ant-tabs-nav-wrap {
        overflow: visible;
        .ant-tabs-nav-scroll {
          overflow: visible;
          .ant-tabs-nav {
            .ant-tabs-ink-bar {
              background-color: transparent;
              &::after {
                content: '';
                position: absolute;
                border: 15px solid transparent;
                border-top: 15px solid ${colors.black};
                top: 100%;
                left: 50%;
                transform: translateX(-50%) translateY(-10%);
              }
            }
            .ant-tabs-tab {
              color: ${colors.darkGray};
              padding: 12px 20px;
              &.ant-tabs-tab-active, &:hover {
                color: ${colors.lightGray};
              }
            }
          }
        }
      }
    }
  }
`
// endregion

class RBTabs extends React.Component {
  static propTypes = {
    children: React.PropTypes.array.isRequired
  }

  _renderTabs() {
    return this.props.children.map((item, i) => {
      const tabDOM = React.cloneElement(item, {ref: i})
      const j = i + 1
      return (
        <TabPane {...tabDOM.props} key={j}>
          {tabDOM.props.children}
        </TabPane>
      )
    })
  }

  render() {
    return (
      <Main>
        {this._renderTabs()}
      </Main>
    )
  }
}

export default RBTabs

// <RBTabs>
// {React.Children.toArray([
//   <div tab='tab 1' icon='mail'>content tab 1</div>,
//   <div tab='tab 2'>content tab 2</div>
// ])}
// </RBTabs>
