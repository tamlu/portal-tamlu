/*
 *
 * Authorization
 *
 * this component connects the redux state user to the
 */

import React, {PropTypes} from 'react'
import {createSelector} from 'reselect'
import {connect} from 'react-redux'
import omit from 'lodash/omit'
import {List} from 'immutable'

import {makeSelectRoles} from './selectors'

function withAuthorization(WrappedComponent, allowedRoles) {
  class ComponentWithAuthorization extends React.Component {
    constructor({roles}) {
      super()
      this.state = {
        roles,
        authorized: allowedRoles.some(role => roles.includes(role))
      }
    }

    render() {
      const {authorized} = this.state
      return authorized
        ? <WrappedComponent {...omit(this.props, 'roles')} />
        : null
    }
  }

  ComponentWithAuthorization.propTypes = {
    roles: PropTypes.instanceOf(List)
  }

  const mapStateToProps = createSelector(makeSelectRoles(), roles => ({
    roles
  }))

  return connect(mapStateToProps)(ComponentWithAuthorization)
}

export default withAuthorization
