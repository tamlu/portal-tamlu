import {loadUser} from '../actions'

import {LOAD_USER} from '../constants'

describe('Authorization actions', () => {
  describe('Load User Action', () => {
    it('updates userName and roles', () => {
      const userInfo = {
        userName: 'Tan',
        roles: ['admin']
      }

      const expected = {
        type: LOAD_USER,
        ...userInfo
      }
      expect(loadUser(userInfo)).toEqual(expected)
    })
  })
})
