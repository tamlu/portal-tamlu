import React from 'react'
import {shallow} from 'enzyme'

import configureStore from 'tests/configureMockStore'
import withAuthorization from '../index'

describe('withAuthorization', () => {
  const initialState = {
    authorization: {
      roles: ['admin']
    }
  }

  const store = configureStore(initialState)

  const WrappedComponent = () => <div />

  const renderComponent = roles => {
    const HocComponent = withAuthorization(WrappedComponent, roles)

    return shallow(<HocComponent />, {context: {store}})
  }

  describe('user has role to use the component', () => {
    it('renders inner component once', () => {
      const renderedComponent = renderComponent(['admin'])
      expect(renderedComponent.dive().find(WrappedComponent)).toHaveLength(1)
    })
  })

  describe('user does NOT has role to use the component', () => {
    it('does NOT renders inner component', () => {
      const renderedComponent = renderComponent(['user'])
      expect(renderedComponent.dive().find(WrappedComponent)).toHaveLength(0)
    })
  })
})
