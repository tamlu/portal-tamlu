import {fromJS} from 'immutable'

import {
  selectAuthorization,
  makeSelectRoles,
  makeSelectUserName
} from '../selectors'

describe('selectAuthorization', () => {
  it('selects the global state', () => {
    const globalState = fromJS({})
    const mockedState = fromJS({
      authorization: globalState
    })
    expect(selectAuthorization(mockedState)).toEqual(globalState)
  })
})

describe('makeSelectRoles', () => {
  it('selects the roles state', () => {
    const globalState = fromJS({
      roles: ['admin']
    })

    const mockedState = fromJS({
      authorization: globalState
    })

    const selectRoles = makeSelectRoles()

    expect(selectRoles(mockedState)).toEqual(fromJS(['admin']))
  })
})

describe('makeSelectUserName', () => {
  it('selects the roles state', () => {
    const globalState = fromJS({
      userName: 'Tan'
    })

    const mockedState = fromJS({
      authorization: globalState
    })

    const selectUserName = makeSelectUserName()

    expect(selectUserName(mockedState)).toEqual(fromJS('Tan'))
  })
})
