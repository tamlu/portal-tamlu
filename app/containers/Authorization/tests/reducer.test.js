import {fromJS} from 'immutable'

import authorizationReducer from '../reducer'
import {LOAD_USER} from '../constants'

describe('authorizationReducer', () => {
  it('returns the initial state', () => {
    expect(authorizationReducer(undefined, {})).toEqual(
      fromJS({
        userName: null,
        roles: ['admin']
      })
    )
  })

  it('changes the user info', () => {
    const userInfo = {
      userName: 'Tan',
      roles: ['admin']
    }

    expect(
      authorizationReducer(undefined, {type: LOAD_USER, ...userInfo}).toJS()
    ).toEqual(userInfo)
  })
})
