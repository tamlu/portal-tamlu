/*
 *
 * LanguageProvider actions
 *
 */

import {createRequestActions, createAction} from 'utils/actionHelpers'

import {
  LOGIN_USER,
  SIGNUP_USER,
  LOAD_ACCESS_TOKEN_FAILED,
  LOAD_ACCESS_TOKEN_SUCCESSFUL
} from './constants'

export const loginUserActions = createRequestActions(LOGIN_USER)

export const signupUserActions = createRequestActions(SIGNUP_USER)

export const loadAccessTokenFailed = () =>
  createAction(LOAD_ACCESS_TOKEN_FAILED)

export const loadAccessTokenSucceed = () =>
  createAction(LOAD_ACCESS_TOKEN_SUCCESSFUL)
