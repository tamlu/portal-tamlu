/*
 *
 * LanguageProvider reducer
 *
 */

import {fromJS} from 'immutable'
import {combineReducers} from 'redux-immutable'
import pick from 'lodash/pick'

import {
  LOAD_ACCESS_TOKEN_FAILED,
  LOGIN_USER,
  LOAD_ACCESS_TOKEN_SUCCESSFUL
} from './constants'

function token(state = fromJS({}), action) {
  switch (action.type) {
    case LOGIN_USER.SUCCESS:
      return fromJS(
        pick(action.response, ['accessToken', 'expiresIn', 'expiresAt'])
      )
    case LOGIN_USER.FAILURE:
      return fromJS({})
    case LOAD_ACCESS_TOKEN_FAILED:
      return fromJS({})
    case LOAD_ACCESS_TOKEN_SUCCESSFUL:
      return state
    default:
      return state
  }
}

export default combineReducers({
  token
})
