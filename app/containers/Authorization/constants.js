/*
 *
 * LanguageProvider constants
 *
 */
import {createRequestTypes} from 'utils/actionHelpers'

export const LOGIN_USER = createRequestTypes('app/Authorization/LOGIN_USER')
export const SIGNUP_USER = createRequestTypes('app/Authorization/SIGNUP_USER')
export const LOAD_ACCESS_TOKEN_FAILED = 'app/App/LOAD_ACCESS_TOKEN_FAILED'
export const LOAD_ACCESS_TOKEN_SUCCESSFUL =
  'app/App/LOAD_ACCESS_TOKEN_SUCCESSFUL'
