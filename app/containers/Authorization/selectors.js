import {createSelector} from 'reselect'

/**
 * Direct selector to the authorization state domain
 */
const makeselectAuthorization = () =>
  createSelector(
    state => state.get('global'),
    global => global.get('authorization')
  )

const makeSelectTokenObject = () =>
  createSelector(makeselectAuthorization(), authorization =>
    authorization.get('token')
  )
const makeSelectStrToken = () =>
  createSelector(makeSelectTokenObject(), token => token.get('accessToken'))
const makeSelectAuthenticated = () =>
  createSelector(makeSelectStrToken(), accessToken => !!accessToken)

export {
  makeselectAuthorization,
  makeSelectTokenObject,
  makeSelectStrToken,
  makeSelectAuthenticated
}
