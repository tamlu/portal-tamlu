import {takeLatest, fork, put} from 'redux-saga/effects'
import {
  LOGIN_USER,
  SIGNUP_USER,
  LOAD_ACCESS_TOKEN_FAILED
} from 'containers/Authorization/constants'
import {
  loginUserActions,
  signupUserActions
} from 'containers/Authorization/actions'
import {push} from 'react-router-redux'

import sendRequest from 'utils/sendRequest'
import {postLoginUser, postSignupUser} from 'utils/api'

const sendRequestLoginUser = sendRequest.bind(
  null,
  loginUserActions,
  postLoginUser
)
const sendRequestSignupUser = sendRequest.bind(
  null,
  signupUserActions,
  postSignupUser
)

function* loginUser({payload}) {
  yield fork(sendRequestLoginUser, payload)
}

function* signupUser({payload}) {
  yield fork(sendRequestSignupUser, payload)
}

export function* user() {
  yield takeLatest(LOGIN_USER.INITIATED, loginUser)
  yield takeLatest(SIGNUP_USER.INITIATED, signupUser)
}

export function* redirectNotAuthorized() {
  yield takeLatest(LOAD_ACCESS_TOKEN_FAILED, redirectTologin)
}

export function* redirectTologin() {
  yield put(push('/login'))
}

export function* redirectUserToRoot() {
  yield put(push('/apply'))
}

export function* redirectLoggedinUser() {
  yield takeLatest(LOGIN_USER.SUCCESS, redirectUserToRoot)
}

export default [user, redirectLoggedinUser, redirectNotAuthorized]
