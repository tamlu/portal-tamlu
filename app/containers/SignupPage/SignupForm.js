/*
 *
 * SignupForm
 *
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {compose} from 'redux'
import {reduxForm} from 'redux-form/immutable'
import {createStructuredSelector} from 'reselect'

import styled from 'styled-components'
import {Form, Input, Button} from 'antd'

import {colors, typeface} from '../../utils/styles'

const FormItem = Form.Item

const Main = styled(Form)`
  border: solid 1px ${colors.lightGray};
  background-color: ${colors.white};
  border-radius: 5px;
  width: 100%;
  max-width: 400px;
  margin: auto;
  
  .ant-input, .ant-btn {
    font-size: ${typeface.size.normal};
    height: 40px;
  }
  .ant-btn-primary {
    background-color: ${colors.pink};
    border-color: ${colors.pink};
    &:hover, &:focus {
      background-color: ${colors.darkPink};
      border-color: ${colors.darkPink};
    }
  }
`
const Head = styled.div`
  border-bottom: solid 1px ${colors.mediumGray};
  padding: 20px 40px;
  background-color: ${colors.ecruWhite};
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  
  div {
    font-size: ${typeface.size.small};
    font-weight: 700;
    color: ${colors.black};
    text-align: center;
    .orange {
      color: ${colors.orange};
    }
  }
`
const Body = styled.div`
  padding: 20px 40px;
  text-align: center;
  .ant-input {
    font-size: ${typeface.size.normal}
  }
  p {
    margin-bottom: 15px;
  }
  .ant-form-item:last-child, p:last-child {
    margin-bottom: 0;
  }
`
const BodyTitle = styled.div`
  font-size: ${typeface.size.normal}
  font-weight: 700;
  text-align: center;
  border-bottom: solid 1px ${colors.lightGray};
`
const Foot = styled.div`
  border-top: solid 1px ${colors.mediumGray};
  padding: 20px 40px;
  text-align: center;
`

// eslint-disable-next-line react/prefer-stateless-function
export class SignupForm extends React.PureComponent {
  render() {
    // const {handleSubmit} = this.props

    return (
      <Main className='box-container'>
        <Head>
          <div>
            Sign Up Form, <span className='orange'> Some text in orange</span>
          </div>
        </Head>
        <Body className='body'>
          <FormItem>
            <BodyTitle>Sign Up Form</BodyTitle>
          </FormItem>
          <FormItem>
            <Input placeholder='Account' />
          </FormItem>
          <FormItem>
            <Input placeholder='Email' />
          </FormItem>
          <FormItem>
            <Input placeholder='Password' />
          </FormItem>
          <FormItem>
            <Input placeholder='Confirm Password' />
          </FormItem>
        </Body>
        <Foot className='foot'>
          <Button type='primary' htmlType='submit'>
            Sign Up
          </Button>
        </Foot>
      </Main>
    )
  }
}

SignupForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired
}

const mapStateToProps = createStructuredSelector({})

function mapDispatchToProps() {
  return {}
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'signup'
  })
)(SignupForm)
