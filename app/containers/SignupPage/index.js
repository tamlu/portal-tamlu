/*
 *
 * SignupPage
 *
 */

import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Helmet from 'react-helmet'
import {createStructuredSelector} from 'reselect'
import styled from 'styled-components'
import {Row, Col} from 'antd'

import {signupUserActions} from 'containers/Authorization/actions'
// import makeSelectSignupPage from './selectors'
import SignupForm from './SignupForm'

const Main = styled.div`
  background-color: #FBF8D9;
  min-height: 300px;
  padding: 100px 20px;
`

// eslint-disable-next-line react/prefer-stateless-function
export class SignupPage extends React.PureComponent {
  render() {
    return (
      <Main className='signup-page'>
        <Helmet
          title='Signup'
          meta={[{name: 'description', content: 'Description of SignupPage'}]}
        />
        <Row type='flex'>
          <Col span={24}>
            <SignupForm />
          </Col>
        </Row>
      </Main>
    )
  }
}

SignupPage.propTypes = {
  signup: PropTypes.func.isRequired
}

const mapStateToProps = createStructuredSelector(
  {
    // SignupPage: makeSelectSignupPage()
  }
)

function mapDispatchToProps(dispatch) {
  return {
    signup: payload => dispatch(signupUserActions.initiate({payload}))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage)
