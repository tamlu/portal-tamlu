/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react'
import styled from 'styled-components'
import Header from 'components/Header'
import Footer from 'components/Footer'
import withProgressBar from 'components/ProgressBar'

const AppWrapper = styled.div`
  position: relative;
  margin-top: 55px;
`

export class App extends React.PureComponent {
  static propTypes = {
    children: React.PropTypes.node
  }

  render() {
    return (
      <AppWrapper>
        <Header />
        <div id='contents'>
          {React.Children.toArray(this.props.children)}
        </div>
        <Footer />
      </AppWrapper>
    )
  }
}

App.propTypes = {
  children: React.PropTypes.node
}

export default withProgressBar(App)
