import authorizationSagas from 'containers/Authorization/sagas'
import {makeSelectTokenObject} from 'containers/Authorization/selectors'
import {select, put, call} from 'redux-saga/effects'
import {
  loadAccessTokenFailed,
  loadAccessTokenSucceed
} from 'containers/Authorization/actions'
import {changeLocale} from 'containers/LanguageProvider/actions'
import {isExpired} from 'utils'
import {showNotificationAction} from './actions'

export function* getAccessToken() {
  const accessToken = yield select(makeSelectTokenObject())
  if (isExpired(accessToken.get('expiresAt'))) {
    yield put(loadAccessTokenFailed())
    return
  }
  yield put(loadAccessTokenSucceed())
}

export function* checkLogin() {
  yield call(getAccessToken)
}

export function* showNotification(payload) {
  yield put(showNotificationAction(payload))
}

export function* changeLocaleSaga() {
  yield put(changeLocale('ja'))
}

export default [].concat(authorizationSagas, changeLocaleSaga)
