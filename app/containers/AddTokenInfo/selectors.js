import {createSelector} from 'reselect'

/**
 * Direct selector to the addTokenInfo state domain
 */
const selectAddTokenInfoDomain = () => state => state.get('addTokenInfo')

/**
 * Other specific selectors
 */

/**
 * Default selector used by AddTokenInfo
 */

const makeSelectAddTokenInfo = () =>
  createSelector(selectAddTokenInfoDomain(), substate => substate.toJS())

export default makeSelectAddTokenInfo
export {selectAddTokenInfoDomain}
