/*
 *
 * AddTokenInfo
 *
 */

import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Helmet from 'react-helmet'
// import {FormattedMessage} from 'react-intl';
import {createStructuredSelector} from 'reselect'
import makeSelectAddTokenInfo from './selectors'
// import messages from './messages';

import TokenInfo from './TokenInfo'

export class AddTokenInfo extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title='AddTokenInfo'
          meta={[
            {name: 'description', content: 'Description of AddTokenInfo'}
          ]}
        />
        <TokenInfo />
      </div>
    )
  }
}

AddTokenInfo.propTypes = {
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = createStructuredSelector({
  AddTokenInfo: makeSelectAddTokenInfo()
})

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTokenInfo)
