import {fromJS} from 'immutable'
import addTokenInfoReducer from '../reducer'

describe('addTokenInfoReducer', () => {
  it('returns the initial state', () => {
    expect(addTokenInfoReducer(undefined, {})).toEqual(fromJS({}))
  })
})
