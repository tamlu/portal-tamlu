/*
 * AddTokenInfo Messages
 *
 * This contains all the text for the AddTokenInfo component.
 */
import {defineMessages} from 'react-intl'

export default defineMessages({
  header: {
    id: 'app.containers.AddTokenInfo.header',
    defaultMessage: 'This is AddTokenInfo container !'
  }
})
