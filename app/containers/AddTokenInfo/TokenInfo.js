/* eslint-disable brace-style */

import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {compose} from 'redux'
import {createStructuredSelector} from 'reselect'
import {
  Icon,
  Button,
  Row,
  Col,
  Input,
  Form,
  Upload,
  Checkbox,
  Radio,
  message
} from 'antd'
import styled from 'styled-components'

// relative imports
import RBTabs from '../../components/RBTabs/index'
import {colors} from '../../utils/styles/index'

// region ---------------------------------------------------------------------------------------------------- css
const Main = styled.div`
  border: solid 1px ${colors.mediumGray};
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  
  .ant-tabs-bar {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 0;
  }
  .ant-tabs-content {
    background-color: ${colors.white};
  }
  .ant-tabs-bar .ant-tabs-nav-container .ant-tabs-nav-wrap .ant-tabs-nav-scroll .ant-tabs-nav .ant-tabs-tab.ant-tabs-tab-active {
    padding: 20px 40px;
  }
`
const Top = styled.div`
  border-bottom: solid 1px ${colors.mediumGray};
  padding: 20px 40px;
  display: flex;
  justify-content: flex-end;
  
  
`
const Mid = styled.div`
  padding: 20px 40px;
  
  > .ant-row {
    border-bottom: solid 1px ${colors.mediumGray};
    margin-bottom: 24px;
    &:last-child, &:first-child {
      border-bottom: none;
      margin-bottom: 0;
    }
    
    .radio-button {
      .ant-radio-group-large .ant-radio-button-wrapper {
        border: 1px solid #d9d9d9;
        margin-right: 8px;
        margin-bottom: 8px;
        border-radius: 4px;
        
        &.ant-radio-button-wrapper-checked, &:hover {
          border-color: #108ee9;
          box-shadow: none;
        }
      }
    }
    
    .ant-upload-list-item {
      i {
        display: none;
      }
    }
    
    .preview-item {
      img {
        height: 100%;
        max-height: 180px;
      }
    }
  }
`
const Bottom = styled.div`
  border-top: solid 1px ${colors.mediumGray};
  padding: 20px 40px;
  text-align: center;
  
  .ant-btn-primary {
    background-color: ${colors.pink};
    border-color: ${colors.pink};
    &:hover, &:focus {
      background-color: ${colors.darkPink};
      border-color: ${colors.darkPink};
    }
  }
`
// endregion

const FormItem = Form.Item
const RadioButton = Radio.Button
const RadioGroup = Radio.Group
const InputGroup = Input.Group

// const SPONSEE_CATEGORY = enums.SPONSEE_CATEGORY;

function getBase64(img, callback) {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result))
  reader.readAsDataURL(img)
}

function beforeUpload(file) {
  const isImageFile = file.type === 'image/jpeg' || file.type === 'image/png'
  if (!isImageFile) {
    message.error('You can only upload JPG file!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!')
  }
  return isImageFile && isLt2M
}

class TokenInfo extends React.Component {
  static propTypes = {
    form: PropTypes.object.isRequired
  }

  state = {
    imageUrl: '',
    fileList: [],
    confirmDirty: false,
    extraInput1Disabled: true,
    extraInput2Disabled: true
  }

  handleChangeImage = info => {
    let fileList = info.fileList
    fileList = fileList.slice(-1)
    this.setState({fileList})

    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({imageUrl})
      )
    }
  }
  handleConfirmBlur = e => {
    const value = e.target.value
    this.setState({confirmDirty: this.state.confirmDirty || !!value})
  }
  checkConfirmEmail = (rule, value, callback) => {
    const form = this.props.form
    if (value && value !== form.getFieldValue('email')) {
      callback('Not the same as previous e-mail address!')
    } else {
      callback()
    }
  }
  validateForm = e => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err /*values*/) => {
      if (!err) {
        // console.log('Received values of form: ', values);
      }
    })
  }
  handleCheckExtraInput1 = e => {
    const value = e.target.checked
    this.setState({extraInput1Disabled: !value})
  }
  handleCheckExtraInput2 = e => {
    const value = e.target.checked
    this.setState({extraInput2Disabled: !value})
  }

  render() {
    const {getFieldDecorator} = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 6}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 14}
      }
    }
    const imageUrl = this.state.imageUrl

    return (
      <Main>
        <Form>
          <RBTabs>
            {React.Children.toArray([
              <div tab=' Create of coin' icon='mail'>
                <Top className='top'>
                  <Button type='default' htmlType='button'>Back</Button>
                </Top>
                <Mid>
                  <Row>
                    <Col xs={24} md={12}>
                      <FormItem {...formItemLayout} label='Dragger' hasFeedback>
                        <div className='dropbox'>
                          {getFieldDecorator('dragger', {
                            // valuePropName: 'fileList',
                            // getValueFromEvent: this.normFile,
                            rules: [
                              {
                                required: true,
                                message: 'Cannot be empty!'
                              }
                            ]
                          })(
                            <Upload.Dragger
                              name='files'
                              action=''
                              beforeUpload={beforeUpload}
                              onChange={this.handleChangeImage}
                              accept='image/*'
                              fileList={this.state.fileList}
                            >
                              <p className='ant-upload-drag-icon'>
                                <Icon type='inbox' />
                              </p>
                              <p className='ant-upload-text'>
                                Click or drag file to this area to upload
                              </p>
                              <p className='ant-upload-hint'>
                                Support for a single or bulk upload. Strictly
                                prohibit
                                from uploading company data or other
                                band files
                              </p>
                            </Upload.Dragger>
                          )}
                        </div>
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        className='preview-item'
                        {...formItemLayout}
                        label=' '
                        colon={false}
                      >
                        {imageUrl ? <img src={imageUrl} alt='Preview' /> : ''}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Sponsee Name'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('sponseeName', {
                          rules: [
                            {
                              required: true,
                              message: 'Cannot be empty!'
                            }
                          ]
                        })(<Input />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        className='radio-button'
                        {...formItemLayout}
                        label='Category'
                      >
                        {getFieldDecorator('category', {
                          initialValue: 'group',
                          rules: []
                        })(
                          <RadioGroup>
                            <RadioButton value='male'>Male</RadioButton>
                            <RadioButton value='female'>Female</RadioButton>
                            <RadioButton value='group'>Group</RadioButton>
                            <RadioButton value='company'>Company</RadioButton>
                          </RadioGroup>
                        )}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Coin Name'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('coinName', {
                          rules: [
                            {
                              min: 3,
                              max: 10
                            },
                            {
                              required: true,
                              message: 'Cannot be empty!'
                            }
                          ]
                        })(<Input />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Coin Symbol'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('coinSymbol', {
                          rules: [
                            {
                              min: 3,
                              max: 10
                            },
                            {
                              required: true,
                              message: 'Cannot be empty!'
                            }
                          ]
                        })(<Input />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem label='E-mail' {...formItemLayout} hasFeedback>
                        {getFieldDecorator('email', {
                          rules: [
                            {
                              type: 'email',
                              message: 'Invalid E-mail!'
                            },
                            {
                              required: true,
                              message: 'Cannot be empty!'
                            }
                          ]
                        })(<Input />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Confirm E-mail'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('emailConfirm', {
                          rules: [
                            {
                              required: true,
                              message: 'Cannot be empty!'
                            },
                            {
                              validator: this.checkConfirmEmail
                            }
                          ]
                        })(<Input onBlur={this.handleConfirmBlur} />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Zip Code'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('zipCode', {
                          rules: []
                        })(<Input />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Sponsee Address'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('sponseeAddress', {
                          rules: []
                        })(<Input />)}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Phone Number'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('phoneNumber', {
                          rules: []
                        })(<Input />)}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Introduction'
                        {...formItemLayout}
                        hasFeedback
                      >
                        {getFieldDecorator('introduction', {
                          rules: []
                        })(<Input type='textarea' />)}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={24} md={12}>
                      <FormItem
                        label='Usage of support money from sponsor'
                        {...formItemLayout}
                      >
                        {getFieldDecorator('moneyUsage', {
                          rules: []
                        })(
                          <div>
                            <Checkbox>活動費</Checkbox><br />
                            <Checkbox>CD制作</Checkbox><br />
                            <Checkbox>動画制作</Checkbox><br />
                            <Checkbox>ライブ費用</Checkbox><br />
                            <Checkbox>機材購入</Checkbox><br />
                            <InputGroup compact>
                              <Checkbox onChange={this.handleCheckExtraInput1}>
                                その他
                              </Checkbox>
                              <Col>
                                <Input
                                  disabled={this.state.extraInput1Disabled}
                                />
                              </Col>
                            </InputGroup>
                          </div>
                        )}
                      </FormItem>
                    </Col>
                    <Col xs={24} md={12}>
                      <FormItem
                        className='has-long-label'
                        label='Sponsors spend sponsee coin'
                        {...formItemLayout}
                      >
                        {getFieldDecorator('moneyUsage', {
                          rules: []
                        })(
                          <Col>
                            <Checkbox>楽曲ダウンロード</Checkbox><br />
                            <Checkbox>写真集ダウンロード</Checkbox><br />
                            <Checkbox>音声ダウンロード</Checkbox><br />
                            <Checkbox>動画ダウンロード</Checkbox><br />
                            <Checkbox>チケットやイベントの優先特典</Checkbox><br />
                            <Checkbox>グッズ等の購入費</Checkbox><br />
                            <Checkbox>活動報告写真の閲覧条件</Checkbox><br />
                            <InputGroup compact>
                              <Checkbox onChange={this.handleCheckExtraInput2}>
                                その他
                              </Checkbox>
                              <Col>
                                <Input
                                  disabled={this.state.extraInput2Disabled}
                                />
                              </Col>
                            </InputGroup>
                          </Col>
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                </Mid>
                <Bottom>
                  <Button
                    type='primary'
                    htmlType='button'
                    onClick={this.validateForm}
                  >
                    Next
                  </Button>
                </Bottom>
              </div>
            ])}
          </RBTabs>
        </Form>
      </Main>
    )
  }
}

const mapStateToProps = createStructuredSelector(
  {
    // applyData: makeSelectApplyData()
  }
)
function mapDispatchToProps(/*dispatch*/) {
  return {}
}
export default compose(
  Form.create(),
  connect(mapStateToProps, mapDispatchToProps)
)(TokenInfo)
