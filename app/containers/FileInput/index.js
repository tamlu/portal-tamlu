/*
 *
 * FileInput
 *
 */

import React, {PropTypes} from 'react'
import {connect} from 'react-redux'

function uploadFile(file, url) {
  return fetch(url, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': file.type
    }),
    body: file
  })
}

function uploadToFileS3(file) {
  return fetch(`http://localhost:4000/users/sign-avatar`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      fileType: file.type,
      fileName: file.name
    })
  })
    .then(response => response.json())
    .then(json => uploadFile(file, json.uploadUrl).then(() => json.downloadUrl))
}

export class FileInput extends React.Component {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props)
    this.state = {image: null}
  }

  render() {
    return (
      <div>
        {this.state.image && <img src={this.state.image} alt='te' />}
        <input
          type='file'
          name='file'
          id='file-input'
          ref={input => (this.input = input)}
          onChange={() => {
            uploadToFileS3(this.input.files[0]).then(image =>
              this.setState({image})
            )
          }}
        />
      </div>
    )
  }
}

FileInput.propTypes = {
  dispatch: PropTypes.func.isRequired
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(null, mapDispatchToProps)(FileInput)
