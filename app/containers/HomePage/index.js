/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'

class HomePage extends React.PureComponent {
  // eslint-disable-next-line react/prefer-stateless-function
  render() {
    return <div />
  }
}

HomePage.propTypes = {}

const mapStateToProps = createStructuredSelector({})

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
