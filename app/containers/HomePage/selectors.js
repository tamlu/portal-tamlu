import {createSelector} from 'reselect'

/**
 * Direct selector to the homePage state domain
 */
const selectHomePageDomain = () => state => state.get('homePage')

/**
 * Other specific selectors
 */

/**
 * Default selector used by LoginPage
 */

const makeSelectHomePage = () =>
  createSelector(selectHomePageDomain(), substate => substate.toJS())

export {makeSelectHomePage}
