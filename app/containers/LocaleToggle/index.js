import React from 'react'
import {connect} from 'react-redux'
import {createSelector} from 'reselect'
import {Select} from 'antd'
import {appLocales} from '../../i18n'
import {changeLocale} from '../LanguageProvider/actions'
import {makeSelectLocale} from '../LanguageProvider/selectors'

const Option = Select.Option
const LangLabels = {
  ja: '日本語',
  en: 'English',
  zh: '中文'
}
export class LocaleToggle extends React.PureComponent {
  render() {
    const {locale, onLocaleToggle} = this.props

    return (
      <Select
        value={locale}
        dropdownMatchSelectWidth={false}
        onChange={onLocaleToggle}
      >
        {appLocales.map(l =>
          <Option key={l} value={l}>{LangLabels[l]}</Option>
        )}
      </Select>
    )
  }
}

LocaleToggle.propTypes = {
  onLocaleToggle: React.PropTypes.func,
  locale: React.PropTypes.string
}

const mapStateToProps = createSelector(makeSelectLocale(), locale => ({
  locale
}))

export function mapDispatchToProps(dispatch) {
  return {
    onLocaleToggle: l => dispatch(changeLocale(l)),
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocaleToggle)
