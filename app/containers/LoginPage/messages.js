/*
 * LoginPage Messages
 *
 * This contains all the text for the LoginPage component.
 */
import {defineMessages} from 'react-intl'

export default defineMessages({
  header: {
    id: 'app.containers.LoginPage.header',
    defaultMessage: 'Log in'
  },
  email: {
    id: 'app.containers.LoginPage.email',
    defaultMessage: 'Email'
  },
  password: {
    id: 'app.containers.LoginPage.password',
    defaultMessage: 'Password'
  },
  emailRequired: {
    id: 'app.containers.LoginPage.emailRequired',
    defaultMessage: 'Please input your email.'
  },
  emailInvalid: {
    id: 'app.containers.LoginPage.emailInvalid',
    defaultMessage: 'Invalid E-mail'
  },
  passwordRequired: {
    id: 'app.containers.LoginPage.passwordRequired',
    defaultMessage: 'Please input your password.'
  },
  noAccount: {
    id: 'app.containers.LoginPage.noAccount',
    defaultMessage: "Don't have an account? Create new one"
  },
  login: {
    id: 'app.containers.LoginPage.login',
    defaultMessage: 'Log in'
  }
})
