/*
 *
 * LoginPage
 *
 */

import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Helmet from 'react-helmet'
import {createStructuredSelector} from 'reselect'
import styled from 'styled-components'
import {Row, Col} from 'antd'

import {loginUserActions} from 'containers/Authorization/actions'
import LoginForm from './LoginForm'

import {colors} from '../../utils/styles'

const Main = styled.div`
  background-color: ${colors.lightYellow};
  min-height: 300px;
  padding: 100px 20px;
`

// eslint-disable-next-line react/prefer-stateless-function
export class LoginPage extends React.PureComponent {
  static propTypes = {
    login: PropTypes.func.isRequired
  }
  render() {
    const {login} = this.props
    return (
      <Main className='login-page'>
        <Helmet
          title='Login'
          meta={[{name: 'description', content: 'Login'}]}
        />
        <Row type='flex'>
          <Col span={24}>
            <LoginForm login={payload => login(payload)} />
          </Col>
        </Row>
      </Main>
    )
  }
}

const mapStateToProps = createStructuredSelector({})
function mapDispatchToProps(dispatch) {
  return {
    login: payload => dispatch(loginUserActions.initiate({payload}))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)
