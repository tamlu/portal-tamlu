/*
 *
 * LoginPage constants
 *
 */

export const DEFAULT_ACTION = 'app/LoginPage/DEFAULT_ACTION'
export const SIGNIN_PAGE_LOADED = 'app/LoginPage/SIGNIN_PAGE_LOADED'
