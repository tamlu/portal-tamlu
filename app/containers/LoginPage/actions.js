/*
 *
 * LoginPage actions
 *
 */
import {createAction} from 'utils/actionHelpers'
import {SIGNIN_PAGE_LOADED} from './constants'

export const signInPageLoaded = () => createAction(SIGNIN_PAGE_LOADED)
export {loginUserActions} from 'containers/Authorization/actions'
