import {checkLogin} from 'containers/App/sagas'
import {takeLatest, take, cancel} from 'redux-saga/effects'
import {LOCATION_CHANGE} from 'react-router-redux'
import {LOAD_ACCESS_TOKEN_SUCCESSFUL} from 'containers/Authorization/constants'
import {redirectUserToRoot} from 'containers/Authorization/sagas'
import {SIGNIN_PAGE_LOADED} from './constants'

function* watchSignInPageLoaded() {
  const watcher = yield takeLatest(SIGNIN_PAGE_LOADED, checkLogin)
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

function* watchTokenLoadedSucceed() {
  const watcher = yield takeLatest(
    LOAD_ACCESS_TOKEN_SUCCESSFUL,
    redirectUserToRoot
  )
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

export default [watchSignInPageLoaded, watchTokenLoadedSucceed]
