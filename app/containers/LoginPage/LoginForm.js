/*
 *
 * LoginForm
 *
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import {injectIntl, intlShape} from 'react-intl'

import styled from 'styled-components'
import {Form, Input, Button} from 'antd'

import {colors, typeface} from '../../utils/styles'
import {signInPageLoaded} from './actions'
import messages from './messages'
const FormItem = Form.Item

// region ---------- CSS
const Main = styled(Form)`
  border: solid 1px ${colors.lightGray};
  background-color: ${colors.white};
  border-radius: 5px;
  width: 100%;
  max-width: 420px;
  margin: auto;
  
  .ant-input, .ant-btn {
    font-size: ${typeface.size.normal}
    height: 40px;
  }
  .ant-btn-primary {
    background-color: ${colors.pink};
    border-color: ${colors.pink};
    &:hover, &:focus {
      background-color: ${colors.darkPink};
      border-color: ${colors.darkPink};
    }
  }
`
const Body = styled.div`
  padding: 20px 40px;
  text-align: center;
  .ant-input {
    font-size: ${typeface.size.normal}
  }
  .ant-form-item:last-child, p:last-child {
    margin-bottom: 0;
  }
`
const BodyTitle = styled.div`
  font-size: ${typeface.size.normal}
  font-weight: 700;
  text-align: center;  
  border-bottom: solid 1px ${colors.lightGray};
`
const BodyParagraph = styled.p`
  margin-bottom: 15px;
  font-size: ${typeface.size.small}
  // color: ${colors.black};
`
const BodyLink = styled.a`
  // color: ${colors.black};
`
const Foot = styled.div`
  border-top: solid 1px ${colors.mediumGray};
  padding: 20px 40px;
  text-align: center;
`
// endregion

// eslint-disable-next-line react/prefer-stateless-function
export class LoginForm extends React.PureComponent {
  static propTypes = {
    login: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    signInPageLoaded: PropTypes.func,
    intl: intlShape.isRequired
  }
  componentWillMount() {
    this.props.signInPageLoaded()
  }
  handleSubmit = e => {
    const {login} = this.props

    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        login(values)
      }
    })
  }
  render() {
    const {intl: {formatMessage}, form: {getFieldDecorator}} = this.props
    return (
      <Main className='box-container' onSubmit={this.handleSubmit}>
        <Body className='body'>
          <FormItem>
            <BodyTitle>{formatMessage(messages.header)}</BodyTitle>
          </FormItem>
          <FormItem>
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: formatMessage(messages.emailRequired)
                },
                {
                  type: 'email',
                  message: formatMessage(messages.emailInvalid)
                },
              ]
            })(<Input placeholder={formatMessage(messages.email)} />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: formatMessage(messages.passwordRequired)
                }
              ]
            })(
              <Input
                type='password'
                placeholder={formatMessage(messages.password)}
              />
            )}
          </FormItem>
          <FormItem>
            <BodyParagraph>
              <BodyLink>
                {formatMessage(messages.noAccount)}
              </BodyLink>
            </BodyParagraph>
          </FormItem>
        </Body>
        <Foot className='foot'>
          <Button type='primary' htmlType='submit'>
            {formatMessage(messages.login)}
          </Button>
        </Foot>
      </Main>
    )
  }
}

const mapStateToProps = createStructuredSelector({})
function mapDispatchToProps(dispatch) {
  return {
    signInPageLoaded: () => dispatch(signInPageLoaded())
  }
}
const WrappedNormalLoginForm = Form.create()(LoginForm)

export default injectIntl(
  connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm)
)
