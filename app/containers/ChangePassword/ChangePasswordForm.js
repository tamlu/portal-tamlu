import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import styled from 'styled-components'
import {Form, Input, Button} from 'antd'
import {push} from 'react-router-redux'
import {colors, typeface} from '../../utils/styles'
import {makeSelectLoading} from './selectors'
import {makeSelectStrToken} from '../Authorization/selectors'
import {changePasswordAction} from './actions'
const FormItem = Form.Item

const Main = styled(Form)`
  border: solid 1px ${colors.lightGray};
  background-color: ${colors.white};
  border-radius: 5px;
  width: 100%;
  max-width: 426px;
  margin: auto;
  .ant-btn-primary {
    background-color: ${colors.pink};
    border-color: ${colors.pink};
    &:hover, &:focus {
      background-color: ${colors.darkPink};
      border-color: ${colors.darkPink};
    }
  }
`
const Body = styled.div`
  padding: 20px 40px;
`
const BodyTitle = styled.div`
  font-size: ${typeface.size.normal}
  font-weight: 700;
  text-align: center;
  border-bottom: solid 1px ${colors.lightGray};
`
const ForgotLink = styled.a`
  font-size: ${typeface.size.small}
  color: #13acfe;
`
const Foot = styled.div`
  border-top: solid 1px ${colors.mediumGray};
  padding: 20px 40px;
  text-align: center;
  .ant-btn-primary {
    margin-left: 10px
  }
`

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field])
}
// eslint-disable-next-line react/prefer-stateless-function
export class ChangePasswordForm extends React.PureComponent {
  static propTypes = {
    changePassword: PropTypes.func.isRequired,
    cancelChangePassword: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    accessToken: PropTypes.string.isRequired
  }
  state = {
    confirmDirty: false
  }

  onCancelClick = () => {
    this.props.cancelChangePassword()
  }

  handleConfirmBlur = e => {
    const value = e.target.value
    this.setState({confirmDirty: this.state.confirmDirty || !!value})
  }

  handleSubmit = e => {
    const {changePassword, accessToken} = this.props

    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        changePassword({...values, accessToken})
        this.props.form.resetFields()
      }
    })
  }

  checkPassword = (rule, value, callback) => {
    const form = this.props.form
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Two passwords that you enter is inconsistent!')
    }
    else {
      callback()
    }
  }
  checkConfirm = (rule, value, callback) => {
    const form = this.props.form
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirmNewPassword'], {force: true})
    }
    callback()
  }

  render() {
    const {getFieldDecorator, getFieldsError} = this.props.form
    return (
      <Main className='box-container' onSubmit={this.handleSubmit}>
        <Body className='body'>
          <FormItem>
            <BodyTitle>Change Password</BodyTitle>
          </FormItem>
          <FormItem>
            {getFieldDecorator('oldPassword', {
              rules: [
                {required: true, message: 'Please input your old password!'}
              ]
            })(<Input type='password' placeholder='Old Password' />)}
            <ForgotLink href=''>Forgot your password?</ForgotLink>
          </FormItem>

          <FormItem>
            {getFieldDecorator('newPassword', {
              rules: [
                {required: true, message: 'Please input your new password!'},
                {validator: this.checkConfirm}
              ]
            })(<Input type='password' placeholder='New Password' />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('confirmNewPassword', {
              rules: [
                {
                  required: true,
                  message: 'Please confirm your new password!'
                },
                {validator: this.checkPassword}
              ]
            })(
              <Input
                type='password'
                placeholder='Confirm New Password'
                onBlur={this.handleConfirmBlur}
              />
            )}
          </FormItem>
        </Body>
        <Foot className='foot'>
          <Button onClick={this.onCancelClick}>Cancel</Button>
          <Button
            type='primary'
            htmlType='submit'
            disabled={hasErrors(getFieldsError())}
          >
            Change
          </Button>
        </Foot>
      </Main>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  accessToken: makeSelectStrToken()
})
function mapDispatchToProps(dispatch) {
  return {
    changePassword: payload =>
      dispatch(changePasswordAction.initiate({payload})),
    cancelChangePassword: () => dispatch(push('/apply'))
  }
}
const WrappedNormalChangePasswordForm = Form.create()(ChangePasswordForm)

export default connect(mapStateToProps, mapDispatchToProps)(
  WrappedNormalChangePasswordForm
)
