/*
 * ChangePassword Messages
 *
 * This contains all the text for the ChangePassword component.
 */
import {defineMessages} from 'react-intl'

export default defineMessages({
  header: {
    id: 'app.containers.ChangePassword.header',
    defaultMessage: 'This is ChangePassword container !'
  }
})
