import {takeLatest, fork, take, cancel} from 'redux-saga/effects'
import sendRequest from 'utils/sendRequest'
import {LOCATION_CHANGE} from 'react-router-redux'
import {showNotification} from 'containers/App/sagas'
import {postChangePassword} from './api'
import {CHANGE_PASSWORD} from './constants'
import {changePasswordAction} from './actions'

const sendRequestChangePassword = sendRequest.bind(
  null,
  changePasswordAction,
  postChangePassword
)
function* requestChangePassword({payload}) {
  yield fork(sendRequestChangePassword, payload)
}

function* watchChangePassword() {
  const watcher = yield takeLatest(
    CHANGE_PASSWORD.INITIATED,
    requestChangePassword
  )
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

function* watchChangePasswordSuccess() {
  const watcher = yield takeLatest(CHANGE_PASSWORD.SUCCESS, showNotification, {
    message: 'Thanks',
    description: 'Your request has been sent successfully'
  })
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

export default [watchChangePassword, watchChangePasswordSuccess]
