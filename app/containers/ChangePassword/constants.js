import {createRequestTypes} from 'utils/actionHelpers'

export const CHANGE_PASSWORD = createRequestTypes(
  'app/ChangePassword/CHANGE_PASSWORD'
)
