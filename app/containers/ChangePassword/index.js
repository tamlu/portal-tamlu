/*
 *
 * ChangePassword
 *
 */

import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import ChangePasswordForm from './ChangePasswordForm'

const Main = styled.div`
  background-color: #FBF8D9;
  min-height: 300px;
  padding: 100px 20px;
`

export class ChangePassword extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Main>
        <Helmet
          title='ChangePassword'
          meta={[{name: 'Change Password', content: 'Change Password'}]}
        />
        <ChangePasswordForm />
      </Main>
    )
  }
}

export default ChangePassword
