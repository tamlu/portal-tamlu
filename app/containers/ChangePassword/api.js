import {request} from 'utils/request'

export const postChangePassword = ({oldPassword, newPassword, accessToken}) =>
  request('/change-password', {
    method: 'POST',
    body: JSON.stringify({
      oldPassword,
      newPassword
    }),
    headers: new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`
    })
  })
