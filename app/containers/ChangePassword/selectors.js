import {createSelector} from 'reselect'

/**
 * Direct selector to the changePassword state domain
 */
const selectChangePasswordDomain = () => state => state.get('changePassword')

/**
 * Other specific selectors
 */

/**
 * Default selector used by ChangePassword
 */

const makeSelectChangePassword = () =>
  createSelector(selectChangePasswordDomain(), substate => substate.toJS())

const makeSelectLoading = () =>
  createSelector(selectChangePasswordDomain(), substate =>
    substate.get('loading')
  )

export default makeSelectChangePassword
export {selectChangePasswordDomain, makeSelectLoading}
