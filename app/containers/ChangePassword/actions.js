import {createRequestActions} from 'utils/actionHelpers'

import {CHANGE_PASSWORD} from './constants'

export const changePasswordAction = createRequestActions(CHANGE_PASSWORD)
