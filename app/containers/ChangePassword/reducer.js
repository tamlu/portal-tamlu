/*
 *
 * ChangePassword reducer
 *
 */

import {fromJS} from 'immutable'
import {CHANGE_PASSWORD} from './constants'

const initialState = fromJS({loading: false, error: ''})

function changePasswordReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_PASSWORD.INITIATED:
      return state.set('loading', true)

    case CHANGE_PASSWORD.SUCESS:
      return state.set('loading', false).set('error', '')

    case CHANGE_PASSWORD.FAILURE:
      return state.set('loading', false).set('error', action.error)

    default:
      return state
  }
}

// export default combineReducers({
//   changePasswordReducer
// })

export default changePasswordReducer
