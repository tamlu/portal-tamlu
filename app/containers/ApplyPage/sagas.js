import {checkLogin, showNotification} from 'containers/App/sagas'
import {takeLatest, fork, take, cancel} from 'redux-saga/effects'
import sendRequest from 'utils/sendRequest'
import {postRequestToJudge} from 'utils/api'
import {LOCATION_CHANGE} from 'react-router-redux'
import {APPLY_PAGE_LOADED, REQUEST_TO_JUDGE} from './constants'
import {requestToJudgeActions} from './actions'

const sendRequestToJudge = sendRequest.bind(
  null,
  requestToJudgeActions,
  postRequestToJudge
)
function* requestToJudge({payload}) {
  yield fork(sendRequestToJudge, payload)
}

function* watchApplyPageLoaded() {
  const watcher = yield takeLatest(APPLY_PAGE_LOADED, checkLogin)
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}
function* watchRequestToJudge() {
  const watcher = yield takeLatest(REQUEST_TO_JUDGE.INITIATED, requestToJudge)
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

function* watchRequestToJudgeSuccess() {
  const watcher = yield takeLatest(REQUEST_TO_JUDGE.SUCCESS, showNotification, {
    message: 'Thanks',
    description: 'Your request has been sent successfully'
  })
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}

export default [
  watchApplyPageLoaded,
  watchRequestToJudge,
  watchRequestToJudgeSuccess
]
