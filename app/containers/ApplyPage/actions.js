import {createAction, createRequestActions} from 'utils/actionHelpers'
import {APPLY_PAGE_LOADED, REQUEST_TO_JUDGE} from './constants'

export const applyPageLoaded = () => createAction(APPLY_PAGE_LOADED)

export const requestToJudgeActions = createRequestActions(REQUEST_TO_JUDGE)
