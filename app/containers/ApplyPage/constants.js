import {createRequestTypes} from 'utils/actionHelpers'

export const APPLY_PAGE_LOADED = 'app/Apply/APPLY_PAGE_LOADED'
export const REQUEST_TO_JUDGE = createRequestTypes(
  'app/ApplyPage/REQUEST_TO_JUDGE'
)
