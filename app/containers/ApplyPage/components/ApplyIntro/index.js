import React from 'react'
import {Row, Col} from 'antd'
import styled from 'styled-components'
import {colors} from '../../../../utils/styles'
import Img from './intro.svg'

const IntroWrapper = styled(Row)`
  padding: 20px;
  border: 1px solid ${colors.lightGray};
  background-color: ${colors.white};
  border-radius: 5px;
`
const Image = styled.img`
  padding: 20px;
  width: 100%;
  height: 100%;
`
const Header = styled.h2`
  padding: 20px;
`
const ApplyIntro = () =>
  <IntroWrapper type='flex' align='middle'>
    <Col xs={24} sm={11} md={7}>
      <Image src={Img} alt='' />
    </Col>
    <Col xs={24} sm={13} md={17}>
      <Header>
        {`Let's start with REALBOOST (registration free)!`}<br />
        {`Let's apply for the first time to create a coin for returning
          the support money of the supporter in order to support the activity fund.`}
      </Header>
    </Col>
  </IntroWrapper>

export default ApplyIntro
