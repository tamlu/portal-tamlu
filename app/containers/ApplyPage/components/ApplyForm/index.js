import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {compose} from 'redux'
import {createStructuredSelector} from 'reselect'
import {Form, Input, Icon, Row, Col, Button, AutoComplete, Radio} from 'antd'
import {
  FaFacebookSquare,
  FaTwitterSquare,
  FaYoutubeSquare,
  FaInstagram
} from 'react-icons/lib/fa'
import styled from 'styled-components'
import {enums} from 'app-core'
import {applyPageLoaded, requestToJudgeActions} from '../../actions'
import {colors, media} from '../../../../utils/styles'

const FormItem = Form.Item
const AutoCompleteOption = AutoComplete.Option
const RadioButton = Radio.Button
const RadioGroup = Radio.Group
const SPONSEE_CATEGORY = enums.SPONSEE_CATEGORY
const FormWrapper = styled.div`
  margin-top: 40px;
  border: 1px solid ${colors.lightGray};
  background-color: ${colors.white};
  border-radius: 5px;
  ${media.tablet`
    margin-top: 20px;
  `}
`
const FormHeader = styled.div`
  position: relative;
  padding: 20px 40px;
  color: ${colors.lightYellow};
  border-radius: 5px 5px 0 0;
  background-color: ${colors.lightDark};
  ${media.tablet`
    padding: 10px 20px;
  `}
  &:after {
    content: '';
    position: absolute;
    border: 10px solid transparent;
    border-top: 10px solid ${colors.lightDark};
    top: 100%;
    left: 10%;
    transform: translateX(-50%) translateY(-10%);
  }
`
const HeaderIcon = styled(Icon)`
  margin-right: 10px;
  color: ${colors.lightYellow};
`
const Header = styled.h3`
  font-weight: normal;
`
const Divider = styled.hr`
  margin-bottom: 25px;
  border: none;
  border-top: 1px solid ${colors.lightGray};
`
const FormBody = styled.div`
  padding: 25px 25px 5px 25px;
`
const Footer = styled(FormItem)`
  margin: 0 !important;
  text-align: center;
  p {
    padding: 20px;
  }
`
const RequestButton = styled(Button)`
  background-color: ${colors.pink};
  border-color: ${colors.pink};
`

class ApplyForm extends React.Component {
  static propTypes = {
    form: PropTypes.object.isRequired,
    applyPageLoaded: PropTypes.func,
    requestToJudge: PropTypes.func
  }
  state = {
    confirmDirty: false,
    disableSendBtn: false,
    autoCompleteResult: []
  }
  componentWillMount() {
    this.props.applyPageLoaded()
  }

  _handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({
          disableSendBtn: true
        })
        this.props.requestToJudge(values)
      }
    })
  }

  _handleConfirmBlur = e => {
    const value = e.target.value
    this.setState({confirmDirty: this.state.confirmDirty || !!value})
  }

  _checkEmail = (rule, value, callback) => {
    const {form} = this.props
    if (value && value !== form.getFieldValue('email')) {
      callback('Two emails that you enter is inconsistent.')
    }
    else {
      callback()
    }
  }

  _checkConfirm = (rule, value, callback) => {
    const form = this.props.form
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], {force: true})
    }
    callback()
  }

  _handleWebsiteChange = value => {
    let autoCompleteResult
    if (!value) {
      autoCompleteResult = []
    }
    else {
      autoCompleteResult = ['.com', '.org', '.net'].map(
        domain => `${value}${domain}`
      )
    }
    this.setState({autoCompleteResult})
  }

  render() {
    const {form} = this.props
    const {getFieldDecorator} = form
    const {autoCompleteResult, disableSendBtn} = this.state
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 6}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 18}
      }
    }
    const websiteOptions = autoCompleteResult.map(website =>
      <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
    )

    return (
      <FormWrapper>
        <FormHeader>
          <Header>
            <HeaderIcon type='mail' />
            <span>Apply for coin creation</span>
          </Header>
        </FormHeader>
        <Form onSubmit={this._handleSubmit}>
          <FormBody>
            <Row gutter={16}>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='Username'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('username', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your username.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='Your username'
                      addonBefore={<Icon type='user' />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='Category'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('category', {
                    initialValue: SPONSEE_CATEGORY.GROUP,
                    rules: [
                      {
                        required: true,
                        message: 'Please select your category.'
                      }
                    ]
                  })(
                    <RadioGroup>
                      <RadioButton value={SPONSEE_CATEGORY.MALE}>
                        Male
                      </RadioButton>
                      <RadioButton value={SPONSEE_CATEGORY.FEMALE}>
                        Female
                      </RadioButton>
                      <RadioButton value={SPONSEE_CATEGORY.GROUP}>
                        Group
                      </RadioButton>
                      <RadioButton value={SPONSEE_CATEGORY.COMPANY}>
                        Company
                      </RadioButton>
                    </RadioGroup>
                  )}
                </FormItem>
              </Col>
            </Row>
            {form.getFieldValue('category') === SPONSEE_CATEGORY.GROUP &&
              <Row gutter={16}>
                <Col xs={24} md={12}>
                  <FormItem
                    {...formItemLayout}
                    label='Total members'
                    required
                    hasFeedback
                  >
                    {getFieldDecorator('numberOfMember', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input the number of members.'
                        }
                      ]
                    })(
                      <Input
                        type='number'
                        placeholder='The number of members'
                        addonBefore={<Icon type='team' />}
                        style={{width: '100%'}}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col xs={24} md={12} />
              </Row>}
            <Row gutter={16}>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='Email'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('email', {
                    rules: [
                      {
                        type: 'email',
                        message: 'The input is not valid Email.'
                      },
                      {
                        required: true,
                        message: 'Please input your Email.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='sample@realboost.com'
                      addonBefore={<Icon type='mail' />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='Confirm Email'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('confirmEmail', {
                    rules: [
                      {
                        type: 'email',
                        message: 'The input is not valid Email.'
                      },
                      {
                        required: true,
                        message: 'Please confirm your Email.'
                      },
                      {
                        validator: this._checkEmail
                      }
                    ]
                  })(
                    <Input
                      placeholder='sample@realboost.com'
                      addonBefore={<Icon type='mail' />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xs={24} md={12}>
                <FormItem {...formItemLayout} label='Facebook' hasFeedback>
                  {getFieldDecorator('facebook', {
                    rules: [
                      {
                        type: 'url',
                        message: 'The input is not valid URL.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='http://www.facebook.com/username'
                      addonBefore={<FaFacebookSquare />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
              <Col xs={24} md={12}>
                <FormItem {...formItemLayout} label='Twitter' hasFeedback>
                  {getFieldDecorator('twitter', {
                    rules: [
                      {
                        type: 'url',
                        message: 'The input is not valid URL.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='http://www.twitter.com/username'
                      addonBefore={<FaTwitterSquare />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xs={24} md={12}>
                <FormItem {...formItemLayout} label='Instagram' hasFeedback>
                  {getFieldDecorator('instagram', {
                    rules: [
                      {
                        type: 'url',
                        message: 'The input is not valid URL.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='http://www.instagram.com/username'
                      addonBefore={<FaInstagram />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
              <Col xs={24} md={12}>
                <FormItem {...formItemLayout} label='Youtube' hasFeedback>
                  {getFieldDecorator('youtube', {
                    rules: [
                      {
                        type: 'url',
                        message: 'The input is not valid URL.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='http://www.youtube.com/channel/userid'
                      addonBefore={<FaYoutubeSquare />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Divider />
            <Row gutter={16}>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='History Activity'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('history', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your history activity.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='History Activity'
                      addonBefore={<Icon type='clock-circle-o' />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='Operation'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('operation', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input your operation.'
                      }
                    ]
                  })(
                    <Input
                      placeholder='Area of Operation'
                      addonBefore={<Icon type='global' />}
                      style={{width: '100%'}}
                    />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col xs={24} md={12}>
                <FormItem
                  {...formItemLayout}
                  label='Result of Activity'
                  required
                  hasFeedback
                >
                  {getFieldDecorator('resultActivity', {
                    rules: [
                      {
                        required: true,
                        message: 'Please input the result of your activity.'
                      }
                    ]
                  })(
                    <Input
                      type='textarea'
                      placeholder='Result of Activity'
                      addonBefore={<Icon type='clock-circle-o' />}
                      autosize={{minRows: 2, maxRows: 8}}
                    />
                  )}
                </FormItem>
              </Col>
              <Col xs={24} md={12}>
                <FormItem {...formItemLayout} label='URL:'>
                  {getFieldDecorator('website', {
                    rules: [
                      {
                        type: 'url',
                        message: 'The input is not valid URL.'
                      }
                    ]
                  })(
                    <AutoComplete
                      dataSource={websiteOptions}
                      onChange={this._handleWebsiteChange}
                    >
                      <Input
                        placeholder='https://yoursite.com'
                        addonBefore={<Icon type='home' />}
                        style={{width: '100%'}}
                      />
                    </AutoComplete>
                  )}
                </FormItem>
              </Col>
            </Row>
          </FormBody>
          <Divider />
          <Footer>
            <RequestButton
              disabled={disableSendBtn}
              type='primary'
              htmlType='submit'
              size='large'
            >
              Send Request
            </RequestButton>
            <p>
              We will review the pros and cons of creating coins at our company
              standard review.{' '}
              <br />
              We will have a few days for the review. <br />
              We will inform you of the registration email address when the
              review is completed.{' '}
              <br /><br />
              Thank you for your application.
            </p>
          </Footer>
        </Form>
      </FormWrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector(
  {
    // applyData: makeSelectApplyData()
  }
)

function mapDispatchToProps(dispatch) {
  return {
    applyPageLoaded: () => dispatch(applyPageLoaded()),
    requestToJudge: payload =>
      dispatch(requestToJudgeActions.initiate({payload}))
  }
}

export default compose(
  Form.create(),
  connect(mapStateToProps, mapDispatchToProps)
)(ApplyForm)
