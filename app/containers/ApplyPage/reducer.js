import {fromJS} from 'immutable'
import {combineReducers} from 'redux-immutable'

import {REQUEST_TO_JUDGE} from './constants'

function requestToJudge(state = fromJS({}), action) {
  switch (action.type) {
    case REQUEST_TO_JUDGE.SUCCESS:
      return fromJS(...action.response)
    case REQUEST_TO_JUDGE.FAILURE:
      return fromJS({})
    case REQUEST_TO_JUDGE:
      return fromJS({})
    default:
      return state
  }
}

export default combineReducers({
  requestToJudge
})
