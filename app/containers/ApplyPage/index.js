import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import {createStructuredSelector} from 'reselect'
import {media, colors} from '../../utils/styles'
import ApplyIntro from './components/ApplyIntro'
import ApplyForm from './components/ApplyForm'

const ApplyWrapper = styled.section`
  background-color: ${colors.lightYellow};
  padding: 40px;
  ${media.tablet`
    padding: 20px;
  `}
`

export class ApplyPage extends React.Component {
  render() {
    return (
      <div>
        <Helmet
          title='Apply for coin creation'
          meta={[{name: 'description', content: 'Apply for coin creation'}]}
        />
        <ApplyWrapper>
          <ApplyIntro />
          <ApplyForm />
        </ApplyWrapper>
      </div>
    )
  }
}

ApplyPage.propTypes = {
  isAuthenticated: PropTypes.bool
}
const mapStateToProps = createStructuredSelector({})

export default connect(mapStateToProps)(ApplyPage)
