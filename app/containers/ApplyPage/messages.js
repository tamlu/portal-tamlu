/*
 * ApplyPage Messages
 *
 * This contains all the text for the ApplyPage component.
 */
import {defineMessages} from 'react-intl'

export default defineMessages({
  header: {
    id: 'app.containers.ApplyPage.header',
    defaultMessage: 'This is ApplyPage container !'
  }
})
